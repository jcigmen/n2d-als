package com.noobs2d.als.explorer;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ResourcesThumbnailAdapter extends ResourceAdapter {

    private class AdapterFilter extends Filter {

	@SuppressLint("DefaultLocale")
	@Override
	protected FilterResults performFiltering(CharSequence constraint) {
	    FilterResults results = new FilterResults();
	    ArrayList<ResourceItem> matchingValues = new ArrayList<ResourceItem>();
	    if (constraint != null && constraint.toString().length() > 0)
		synchronized (this) {
		    for (int i = 0; i < getCount(); i++)
			if (getItem(i).name.toLowerCase().contains(constraint.toString().toLowerCase()))
			    matchingValues.add(getItem(i));
		    results.count = matchingValues.size();
		    results.values = matchingValues;
		}
	    else
		synchronized (this) {
		    ArrayList<ResourceItem> items = new ArrayList<ResourceItem>();
		    for (int i = 0; i < getCount(); i++)
			items.add(getItem(i));
		    results.count = getCount();
		    results.values = items;
		}
	    return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void publishResults(CharSequence constraint, FilterResults results) {
	    clear();
	    ArrayList<ResourceItem> items = (ArrayList<ResourceItem>) results.values;
	    if (items != null)
		for (int i = 0; i < items.size(); i++)
		    add(items.get(i));
	    notifyDataSetChanged();
	}
    }

    private class GridTag {

	public OpaqueImageView thumbnail;
	public TextView text;
    }

    class OpaqueImageView extends ImageView {

	public OpaqueImageView(Context context) {
	    super(context);
	}

	@Override
	public boolean isOpaque() {
	    return false;
	}
    }

    private ArrayList<ResourceItem> resourceItems;
    private AdapterFilter filter;
    private Context context;

    public ResourcesThumbnailAdapter(Context context) {
	this.context = context;
	resourceItems = new ArrayList<ResourceItem>();
	filter = new AdapterFilter();
    }

    @Override
    public void add(ResourceItem item) {
	resourceItems.add(item);
    }

    @Override
    public void clear() {
	resourceItems = new ArrayList<ResourceItem>();
	resourceItems.clear();
    }

    @Override
    public int getCount() {
	return resourceItems.size();
    }

    public Filter getFilter() {
	return filter;
    }

    @Override
    public ResourceItem getItem(int position) {
	return resourceItems.get(position);
    }

    @Override
    public long getItemId(int position) {
	return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	GridTag tag = new GridTag();
	LinearLayout layout;
	if (convertView == null) { // if it's not recycled, initialize some attributes
	    layout = new LinearLayout(context);
	    layout.setOrientation(LinearLayout.VERTICAL);
	    layout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
	    tag.thumbnail = new OpaqueImageView(context);
	    tag.thumbnail.setLayoutParams(new GridView.LayoutParams(200, 200));
	    tag.thumbnail.setPadding(14, 14, 14, 2);
	    tag.thumbnail.setScaleType(ImageView.ScaleType.FIT_CENTER);
	    tag.thumbnail.layout(0, 0, 200, 200);
	    tag.text = new TextView(context);
	    tag.text.setTextColor(Color.BLACK);
	    tag.text.setGravity(Gravity.CENTER);
	    layout.addView(tag.thumbnail);
	    layout.addView(tag.text);
	    layout.setTag(tag);
	} else
	    layout = (LinearLayout) convertView;
	((GridTag) layout.getTag()).thumbnail.setImageBitmap(getItem(position).getThumbnail());
	((GridTag) layout.getTag()).text.setText(getItem(position).name);
	return layout;
    }
}