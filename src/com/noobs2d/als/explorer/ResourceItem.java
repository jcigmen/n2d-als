package com.noobs2d.als.explorer;

import java.io.File;
import java.io.Serializable;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.noobs2d.als.R;
import com.noobs2d.als.ResourcesScreen;

/**
 * Data structure representing an item that is listed on a {@link ResourcesScreen}.
 * 
 * @author MrUseL3tter
 */
public class ResourceItem implements Serializable {

    private static final long serialVersionUID = -4317464460983970699L;

    public enum ItemType {
	DIRECTORY, PDF, AUDIO, IMAGE, PARENT, HOME
    }

    final public ItemType type;
    final public String path;
    final public String name;
    private Context context;
    private Bitmap thumbnail;

    public ResourceItem(Context context, ItemType type, String path, String name) {
	this.type = type;
	this.path = path;
	this.name = name;
	this.context = context;
	generateThumbnails();
    }

    public Bitmap getThumbnail() {
	return thumbnail;
    }

    @Override
    public String toString() {
	return name;
    }

    private void generateThumbnails() {
	String thumbnailPath = null;
	switch (type) {
	    case HOME:
		thumbnail = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_home);
		break;
	    case AUDIO:
		thumbnail = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_audio);
		break;
	    case DIRECTORY:
		thumbnail = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_directory);
		break;
	    case IMAGE:
		thumbnailPath = path.substring(0, path.length() - 4) + ".thumb";
		if (new File(thumbnailPath).exists())
		    thumbnail = BitmapFactory.decodeFile(thumbnailPath);
		else
		    thumbnail = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_image);
		break;
	    case PARENT:
		thumbnail = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_arrow_up);
		break;
	    case PDF:
		thumbnailPath = path.substring(0, path.length() - 4) + ".thumb";
		if (new File(thumbnailPath).exists())
		    thumbnail = BitmapFactory.decodeFile(thumbnailPath);
		else
		    thumbnail = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_doc);
		break;
	}
    }
}
