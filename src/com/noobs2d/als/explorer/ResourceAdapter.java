package com.noobs2d.als.explorer;

import android.widget.BaseAdapter;

public abstract class ResourceAdapter extends BaseAdapter {

    public abstract void add(ResourceItem item);

    public abstract void clear();

    public @Override
    abstract int getCount();

    @Override
    public abstract ResourceItem getItem(int position);

}
