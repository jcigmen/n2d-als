package com.noobs2d.als.explorer;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;

import com.noobs2d.als.R;
import com.noobs2d.als.explorer.ResourceItem.ItemType;

public class ResourcesThumbnailTab extends Activity {

    private GridView gridView;
    private ResourcesThumbnailAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_grid);

	gridView = (GridView) findViewById(R.id.gridview);
	adapter = new ResourcesThumbnailAdapter(getApplicationContext());
	adapter.add(new ResourceItem(getApplicationContext(), ItemType.PDF, "/mnt/sdcard/als/ALS Reference Materials/ALS Directory/BALS_Directory.pdf", "BALS_Directory.pdf"));
	gridView.setAdapter(adapter);
    }
}
