package com.noobs2d.als.explorer;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.noobs2d.als.R;
import com.noobs2d.als.ResourcesScreen;

/**
 * Custom adapter for {@link ResourcesScreen}.
 * 
 * @author MrUseL3tter
 */
public class ResourcesListAdapter extends ResourceAdapter {

    public static final String HEADER = "HEADER";

    public static final String SUB = "SUB";

    private class AdapterFilter extends Filter {

	@SuppressLint("DefaultLocale")
	@Override
	protected FilterResults performFiltering(CharSequence constraint) {
	    FilterResults results = new FilterResults();
	    ArrayList<ResourceItem> matchingValues = new ArrayList<ResourceItem>();
	    if (constraint != null && constraint.toString().length() > 0)
		synchronized (this) {
		    for (int i = 0; i < getCount(); i++)
			if (getItem(i).name.toLowerCase().contains(constraint.toString().toLowerCase()))
			    matchingValues.add(getItem(i));
		    results.count = matchingValues.size();
		    results.values = matchingValues;
		}
	    else
		synchronized (this) {
		    ArrayList<ResourceItem> items = new ArrayList<ResourceItem>();
		    for (int i = 0; i < getCount(); i++)
			items.add(getItem(i));
		    results.count = getCount();
		    results.values = items;
		}
	    return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void publishResults(CharSequence constraint, FilterResults results) {
	    clear();
	    ArrayList<ResourceItem> items = (ArrayList<ResourceItem>) results.values;
	    if (items != null)
		for (ResourceItem item : items)
		    add(item);
	    notifyDataSetChanged();
	}
    }

    /**
     * Holds the {@link ImageView} Icon, Header and Sub (grayish text) {@link TextView}
     */
    private static class ListRowTag {

	public ImageView icon;
	public TextView headerText;
	public TextView subText;
    }

    private ArrayList<ResourceItem> resourceItems;
    private final Context context;
    private AdapterFilter filter;

    private final LayoutInflater inflater;

    public ResourcesListAdapter(Context context, LayoutInflater inflater) {
	this.context = context;
	this.inflater = inflater;
	resourceItems = new ArrayList<ResourceItem>();
	filter = new AdapterFilter();
    }

    @Override
    public void add(ResourceItem item) {
	resourceItems.add(item);
    }

    @Override
    public void clear() {
	resourceItems.clear();
    }

    @Override
    public int getCount() {
	return resourceItems.size();
    }

    public Filter getFilter() {
	return filter;
    }

    @Override
    public ResourceItem getItem(int position) {
	return resourceItems.get(position);
    }

    @Override
    public long getItemId(int position) {
	return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ListRowTag listRow = null;
	if (convertView == null) {
	    convertView = inflater.inflate(R.layout.screen_list_row, parent, false);
	    listRow = new ListRowTag();
	    listRow.icon = (ImageView) convertView.findViewById(R.id.list_row_icon);
	    listRow.headerText = (TextView) convertView.findViewById(R.id.list_row_header);
	    listRow.subText = (TextView) convertView.findViewById(R.id.list_row_sub);
	    convertView.setTag(listRow);
	} else
	    listRow = (ListRowTag) convertView.getTag();

	ResourceItem item = getItem(position);
	listRow.headerText.setText(item.name);
	Bitmap icon = null;
	switch (item.type) {
	    case HOME:
		icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_home);
		listRow.subText.setText("File Folder");
		break;
	    case DIRECTORY:
		icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_directory);
		listRow.subText.setText("File Folder");
		break;
	    case PDF:
		icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_doc);
		listRow.subText.setText("PDF Document");
		break;
	    case AUDIO:
		icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_audio);
		listRow.subText.setText("Audio File");
		break;
	    case IMAGE:
		icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_image);
		listRow.subText.setText("Image File");
		break;
	    case PARENT:
		icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_arrow_up);
		listRow.headerText.setText(item.name);
		//		    ((ViewGroup) listRow.subText.getParent()).removeView(listRow.subText);
		break;
	}
	listRow.icon.setImageBitmap(Bitmap.createScaledBitmap(icon, 32, 32, false));
	return convertView;
    }
}
