package com.noobs2d.als;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;

/**
 * Used for the splash and the credits screen. Accepts a bundle boolean "AUTO_TRANSITION" which
 * denotes whether this screen will animate (fade in then fade out) and transition into
 * {@link MainActivity}.
 * 
 * @author MrUseL3tter
 */
public class CreditsScreen extends Activity {

    private class SplashRunnable implements Runnable {

	@Override
	public void run() {
	    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
	    startActivity(intent);
	    finish();
	    overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
	}

    }

    private SplashRunnable splashRunnable;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
	// to prevent the OS from thinking the app is hanging
	if (event.getAction() == MotionEvent.ACTION_DOWN)
	    synchronized (splashRunnable) {
		splashRunnable.notifyAll();
	    }
	return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_credits);

	boolean autoTransition = getIntent().getBooleanExtra("AUTO_TRANSITION", false);
	if (autoTransition) {
	    splashRunnable = new SplashRunnable();
	    Handler handler = new Handler();
	    handler.postDelayed(splashRunnable, SplashScreen.SPLASH_MILLIS);
	}
    }

}
