package com.noobs2d.als;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.artifex.mupdfdemo.MuPDFActivity;
import com.noobs2d.als.explorer.ResourceAdapter;
import com.noobs2d.als.explorer.ResourceItem;
import com.noobs2d.als.explorer.ResourceItem.ItemType;
import com.noobs2d.als.explorer.ResourcesListAdapter;
import com.noobs2d.als.explorer.ResourcesThumbnailAdapter;
import com.tekle.oss.android.animation.AnimationFactory;
import com.tekle.oss.android.animation.AnimationFactory.FlipDirection;

/**
 * Standard tab view containing a {@link TextView} with gray layout background, and a
 * {@link ListView}
 * 
 * @author MrUseL3tter
 */
public class ResourcesScreen extends Fragment implements OnQueryTextListener, OnClickListener, OnFocusChangeListener, OnItemClickListener, android.view.View.OnClickListener {

    public static final int PREFERENCES_MAX = 10;
    public static final String PREFERENCES_TAG = "ALS_RECENT";
    public static final String PREFERENCES_COUNT = "ALS_RECENT_COUNT";
    public static final String PREFERENCES_PREFIX = "ALS_RECENT_PREFIX";

    private class DirectoryFileFilter implements FileFilter {

	@Override
	public boolean accept(File file) {
	    return file.isDirectory();
	}

    }

    private class DocumentFileFilter implements FileFilter {

	@Override
	public boolean accept(File file) {
	    if (file.isDirectory())
		return false;
	    String path = file.getAbsolutePath();
	    boolean notAThumbnail = !path.endsWith(".thumb");
	    if ((Utils.fileIsAudio(path) || Utils.fileIsImage(path) || Utils.fileIsPDF(path)) && notAThumbnail)
		return true;
	    return false;
	}

    }

    /** just invokes {@link DynamicObserver#onEvent(int, String)} to re-post the updater */
    private class DynamicObserver extends FileObserver {

	public DynamicObserver(String path, int mask) {
	    super(path, mask);
	}

	@Override
	public void onEvent(int event, String path) {
	    handler.post(updater);
	}
    }

    /** keeps on updating the {@link ListView}. */
    private class ListUpdater implements Runnable {

	@Override
	public void run() {
	    justifyIfThereIsParentItem();
	    collectDirectories();
	    collectFiles();
	    populateGridView();
	    populateListView();
	    setCurrentDirectoryText();
	    justifyBackButtonVisibility();
	}

	/** put all files in the current directory to {@link ResourcesScreen.directories} */
	private void collectDirectories() {
	    directories = currentDirectory.listFiles(new DirectoryFileFilter());
	    if (directories == null)
		directories = new File[0];
	}

	/** put all files in the current directory to {@link ResourcesScreen.files} */
	private void collectFiles() {
	    files = currentDirectory.listFiles(new DocumentFileFilter());
	    if (files == null)
		files = new File[0];
	}

	private void justifyBackButtonVisibility() {
	    boolean notOnTopDirectory = parentDirectory != null;
	    setBackButtonVisible(notOnTopDirectory);
	}

	/** there will only be a parent directory if we are not on the topmost target directory */
	private void justifyIfThereIsParentItem() {
	    if (currentDirectory.getAbsolutePath().equals(getTopMostDirectory().getAbsolutePath()))
		parentDirectory = null;
	    else
		parentDirectory = currentDirectory.getParentFile();
	}

	private void populateGridView() {
	    gridAdapter.clear();
	    if (showRecentlyViewed)
		addItemToAdapter(gridAdapter, Utils.convertStringCollectionToFileArray(recentlyViewedFiles));
	    else {
		for (File directory : directories) {
		    ResourceItem item = new ResourceItem(activity.getApplicationContext(), ItemType.DIRECTORY, directory.getAbsolutePath(), directory.getName());
		    gridAdapter.add(item);
		}
		addItemToAdapter(gridAdapter, files);
	    }
	    gridAdapter.notifyDataSetChanged();
	}

	private void populateListView() {
	    listAdapter.clear();
	    if (showRecentlyViewed)
		addItemToAdapter(listAdapter, Utils.convertStringCollectionToFileArray(recentlyViewedFiles));
	    else {
		for (File directory : directories) {
		    ResourceItem item = new ResourceItem(activity.getApplicationContext(), ItemType.DIRECTORY, directory.getAbsolutePath(), directory.getName());
		    listAdapter.add(item);
		}
		addItemToAdapter(listAdapter, files);
	    }
	    listAdapter.notifyDataSetChanged();
	}
    }

    private class SearchTask extends AsyncTask<String, Void, Void> {

	@Override
	protected Void doInBackground(String... queries) {
	    if (showRecentlyViewed)
		updateListAndGridView();
	    else {
		gridAdapter.clear();
		listAdapter.clear();
		addItemToAdapter(gridAdapter, Utils.convertStringCollectionToFileArray(allFiles));
		addItemToAdapter(listAdapter, Utils.convertStringCollectionToFileArray(allFiles));
	    }
	    listAdapter.getFilter().filter(queries[0]);
	    gridAdapter.getFilter().filter(queries[0]);
	    return null;
	}

	@Override
	protected void onPostExecute(Void result) {
	    setSpinnerVisibility(false);
	    setViewFlipperVisibility(true);
	}

	@Override
	protected void onPreExecute() {
	    setSpinnerVisibility(true);
	    setViewFlipperVisibility(false);
	}

    }

    private Activity activity;
    /** All files from the top directory to the bottom most directory */
    private ArrayList<String> allFiles;
    private ArrayList<String> recentlyViewedFiles;
    private DynamicObserver observer;
    private File[] directories;
    private File[] files;
    private File currentDirectory;
    private File parentDirectory;
    private GridView gridView;
    private Handler handler;
    private ImageButton backButton;
    private ImageButton toggleView;
    private ListUpdater updater;
    private ListView listView;
    private Menu menu;
    private ProgressBar progressBar;
    private ResourcesListAdapter listAdapter;
    private ResourcesThumbnailAdapter gridAdapter;
    private SearchView searchView;
    private String topDirectory;
    private TextView currentDirectoryText;
    private ViewFlipper viewFlipper;
    private boolean listViewMode;
    private boolean showRecentlyViewed;

    @Override
    public void onClick(DialogInterface dialog, int which) {
	activity.finish();
    }

    @Override
    public void onClick(View view) {
	switch (view.getId()) {
	    case R.id.toggleView:
		onToggleButtonClicked();
		break;
	    case R.id.backButton:
		onBackButtonClicked();
		break;
	}
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	this.menu = menu;
	inflater.inflate(R.menu.main, menu);
	initSearchWidget();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	activity = getActivity();
	View view = inflater.inflate(R.layout.screen_resources, container, false);
	if (isSDCardAvailable()) {
	    allFiles = new ArrayList<String>();
	    topDirectory = getArguments().getString("TOP_DIRECTORY");
	    showRecentlyViewed = getArguments().getBoolean("SHOW_RECENTLY_VIEWED", false);
	    backButton = (ImageButton) view.findViewById(R.id.backButton);
	    backButton.setOnClickListener(this);
	    handler = new Handler();
	    progressBar = (ProgressBar) view.findViewById(R.id.searchSpinner);
	    progressBar.getIndeterminateDrawable().setColorFilter(0xBBBBBBBB, android.graphics.PorterDuff.Mode.MULTIPLY);
	    updater = new ListUpdater();
	    currentDirectoryText = (TextView) view.findViewById(R.id.returnPageText);
	    toggleView = (ImageButton) view.findViewById(R.id.toggleView);
	    toggleView.setOnClickListener(this);
	    viewFlipper = (ViewFlipper) view.findViewById(R.id.flipper);
	    initializeListAndGridView(view);
	    setCurrentDirectoryText();
	    loadAllFilesInTopDirectory(getTopMostDirectory());
	    setHasOptionsMenu(true);
	    setBackButtonVisible(false);
	    setSpinnerVisibility(false);
	} else
	    promptSDCardNotAvailable();
	return view;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
	System.out.println("[ResourcesScreen#onFocusChange()]");
	// since this will only be invoked when the focus is lost on the searchview, we only
	// do one thing: revert the contents of the list and grid adapters into the current
	// dir's contents because we changed it during onQueryTextChange(String)
	if (!hasFocus)
	    updateListAndGridView();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	if (parent.equals(listView))
	    onListViewItemClick(position);
	else
	    onGridViewItemClick(position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	if (item.getItemId() == R.id.menu_about) {
	    // show the credits screen
	    Intent intent = new Intent(activity, CreditsScreen.class);
	    startActivity(intent);
	}
	return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
	super.onPause();
	if (searchView != null) {
	    searchView.setOnQueryTextListener(null);
	    searchView.setOnQueryTextFocusChangeListener(this);
	}
    }

    @Override
    public boolean onQueryTextChange(String query) {
	return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
	new SearchTask().execute(query);
	searchView.setIconified(true);
	searchView.clearFocus();
	return false;
    }

    @Override
    public void onResume() {
	System.out.println("[ResourcesScreen] onResume()");
	super.onResume();
	loadRecentlyViewed();
	updateListAndGridView();
	if (searchView != null) {
	    searchView.setOnQueryTextListener(this);
	    searchView.setOnQueryTextFocusChangeListener(this);
	}
    }

    private void addItemToAdapter(ResourceAdapter adapter, File[] files) {
	for (File file : files) {
	    ItemType type = null;
	    if (Utils.fileIsPDF(file.getAbsolutePath()))
		type = ItemType.PDF;
	    else if (Utils.fileIsAudio(file.getAbsolutePath()))
		type = ItemType.AUDIO;
	    else if (Utils.fileIsImage(file.getAbsolutePath()))
		type = ItemType.IMAGE;
	    adapter.add(new ResourceItem(activity.getApplicationContext(), type, file.getAbsolutePath(), file.getName()));
	}
    }

    private File getTopMostDirectory() {
	return new File(Environment.getExternalStorageDirectory() + "/als/" + topDirectory);
    }

    private void initializeListAndGridView(View parentView) {
	currentDirectory = getTopMostDirectory();
	listView = (ListView) parentView.findViewById(R.id.listView);
	listView.setOnItemClickListener(this);
	listView.setTextFilterEnabled(true);
	listAdapter = new ResourcesListAdapter(activity.getApplicationContext(), activity.getLayoutInflater());
	listView.setAdapter(listAdapter);
	gridView = (GridView) parentView.findViewById(R.id.gridview);
	gridView.setOnItemClickListener(this);
	gridAdapter = new ResourcesThumbnailAdapter(activity.getApplicationContext());
	gridView.setAdapter(gridAdapter);
	updateListAndGridView();
	observer = new DynamicObserver(currentDirectory.getAbsolutePath(), FileObserver.CREATE | FileObserver.DELETE);
	observer.startWatching();
    }

    private void initSearchWidget() {
	searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
	searchView.setOnQueryTextListener(this);
	searchView.setOnQueryTextFocusChangeListener(this);
    }

    private boolean isSDCardAvailable() {
	String storageState = Environment.getExternalStorageState();
	return Environment.MEDIA_MOUNTED.equals(storageState) && !Environment.MEDIA_MOUNTED_READ_ONLY.equals(storageState);
    }

    private void loadAllFilesInTopDirectory(File directory) {
	if (directory.isDirectory()) {
	    File files[] = directory.listFiles(new DocumentFileFilter());
	    for (File file : files)
		allFiles.add(file.getAbsolutePath());
	    File directories[] = directory.listFiles(new DirectoryFileFilter());
	    for (File subDirectory : directories)
		loadAllFilesInTopDirectory(subDirectory);
	}
    }

    private void loadRecentlyViewed() {
	if (recentlyViewedFiles != null)
	    recentlyViewedFiles.clear();
	else
	    recentlyViewedFiles = new ArrayList<String>();
	SharedPreferences preferences = activity.getApplicationContext().getSharedPreferences(ResourcesScreen.PREFERENCES_TAG, 0);
	int maxCount = preferences.getInt(ResourcesScreen.PREFERENCES_COUNT, 0);
	for (int i = 0; i < maxCount; i++) {
	    String path = preferences.getString(ResourcesScreen.PREFERENCES_PREFIX + i, "");
	    File file = new File(path);
	    if (file.exists())
		recentlyViewedFiles.add(path);
	}
    }

    private void onBackButtonClicked() {
	toParentDirectory();
    }

    private void onGridViewItemClick(int position) {
	if (gridAdapter.getItem(position).type != ItemType.DIRECTORY) {
	    updateRecentlyViewed(gridAdapter.getItem(position).path);
	    saveRecentlyViewed();
	}
	openResource(gridAdapter.getItem(position));
    }

    private void onListViewItemClick(int position) {
	if (listAdapter.getItem(position).type != ItemType.DIRECTORY) {
	    updateRecentlyViewed(listAdapter.getItem(position).path);
	    saveRecentlyViewed();
	}
	openResource(listAdapter.getItem(position));
    }

    private void onToggleButtonClicked() {
	if (listViewMode) {
	    toggleView.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_grid));
	    AnimationFactory.flipTransition(viewFlipper, FlipDirection.LEFT_RIGHT);
	} else {
	    toggleView.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_list));
	    AnimationFactory.flipTransition(viewFlipper, FlipDirection.LEFT_RIGHT);
	}
	listViewMode = !listViewMode;
    }

    private void openResource(ResourceItem item) {
	Uri uri = Uri.parse(item.path);
	Intent intent = null;
	if (Utils.fileIsPDF(item.path)) {
	    intent = new Intent(activity, MuPDFActivity.class);
	    intent.setAction(Intent.ACTION_VIEW);
	    intent.setData(uri);
	    startActivity(intent);
	} else if (Utils.fileIsImage(item.path)) {
	    intent = new Intent();
	    intent.setAction(Intent.ACTION_VIEW);
	    intent.setDataAndType(Uri.fromFile(new File(item.path)), "image/*");
	    startActivity(intent);
	} else if (Utils.fileIsAudio(item.path)) {
	    intent = new Intent();
	    intent.setAction(Intent.ACTION_VIEW);
	    intent.setDataAndType(Uri.fromFile(new File(item.path)), "audio/*");
	    startActivity(intent);
	} else if (item.type == ItemType.DIRECTORY) {
	    currentDirectory = new File(item.path); // a directory
	    updateListAndGridView();
	}
    }

    private void promptSDCardNotAvailable() {
	AlertDialog.Builder builder = new AlertDialog.Builder(activity);
	builder.setIconAttribute(android.R.attr.alertDialogIcon);
	builder.setCancelable(false);
	builder.setTitle(R.string.prompt_sd_card_unavailable_title);
	builder.setMessage(R.string.prompt_sd_card_unavailable_body);
	builder.setPositiveButton("Close", this);
	AlertDialog alert = builder.create();
	alert.show();
    }

    private void saveRecentlyViewed() {
	// remove redundancies
	for (int i = recentlyViewedFiles.size() - 1; i >= 0; i--)
	    for (int j = 0; j < i; j++)
		if (recentlyViewedFiles.get(i).equals(recentlyViewedFiles.get(j))) {
		    recentlyViewedFiles.remove(i);
		    break;
		}
	for (int i = recentlyViewedFiles.size() - 1; i >= PREFERENCES_MAX; i--)
	    recentlyViewedFiles.remove(i);
	// commit to SharedPreferences
	SharedPreferences.Editor editor = activity.getApplicationContext().getSharedPreferences(PREFERENCES_TAG, 0).edit();
	editor.putInt(PREFERENCES_COUNT, recentlyViewedFiles.size());
	for (int i = 0; i < recentlyViewedFiles.size(); i++)
	    editor.putString(PREFERENCES_PREFIX + i, recentlyViewedFiles.get(i));
	editor.commit();
    }

    private void setBackButtonVisible(boolean visible) {
	if (visible)
	    backButton.setVisibility(View.VISIBLE);
	else
	    backButton.setVisibility(View.GONE);
	backButton.invalidate();
    }

    private void setCurrentDirectoryText() {
	currentDirectoryText.setText(currentDirectory.getName());
    }

    private void setSpinnerVisibility(boolean visible) {
	if (visible)
	    progressBar.setVisibility(View.VISIBLE);
	else
	    progressBar.setVisibility(View.GONE);
    }

    private void setViewFlipperVisibility(boolean visible) {
	if (visible)
	    viewFlipper.setVisibility(View.VISIBLE);
	else
	    viewFlipper.setVisibility(View.GONE);
    }

    private void toParentDirectory() {
	currentDirectory = parentDirectory;
	updateListAndGridView();
    }

    private void updateListAndGridView() {
	handler.post(updater);
    }

    private void updateRecentlyViewed(String path) {
	if (recentlyViewedFiles.size() >= PREFERENCES_MAX)
	    recentlyViewedFiles.remove(recentlyViewedFiles.size() - 1);
	recentlyViewedFiles.add(0, path);
    }
}