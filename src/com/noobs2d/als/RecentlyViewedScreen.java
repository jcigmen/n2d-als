package com.noobs2d.als;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.artifex.mupdfdemo.MuPDFActivity;
import com.noobs2d.als.explorer.ResourceItem;
import com.noobs2d.als.explorer.ResourceItem.ItemType;
import com.noobs2d.als.explorer.ResourcesListAdapter;

public class RecentlyViewedScreen extends Activity implements OnItemClickListener {

    private ListView listView;
    private ResourcesListAdapter adapter;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	ResourceItem item = adapter.getItem(position);
	openResource(item.path);
    }

    private void loadRecentlyViewed() {
	adapter.clear();
	SharedPreferences preferences = getApplicationContext().getSharedPreferences(ResourcesScreen.PREFERENCES_TAG, 0);
	int count = preferences.getInt(ResourcesScreen.PREFERENCES_COUNT, 0);
	for (int i = 0; i < count; i++) {
	    String fileName = preferences.getString(ResourcesScreen.PREFERENCES_PREFIX + i, "");
	    File file = new File(fileName);
	    if (file.exists()) {
		ItemType itemType = null;
		if (Utils.fileIsAudio(fileName))
		    itemType = ItemType.AUDIO;
		else if (Utils.fileIsImage(fileName))
		    itemType = ItemType.IMAGE;
		else if (Utils.fileIsPDF(fileName))
		    itemType = ItemType.PDF;
		adapter.add(new ResourceItem(getApplicationContext(), itemType, fileName, file.getName()));
		adapter.notifyDataSetChanged();
	    }
	}
    }

    private void openResource(String path) {
	Uri uri = Uri.parse(path);
	Intent intent = null;
	if (Utils.fileIsPDF(path)) {
	    intent = new Intent(this, MuPDFActivity.class);
	    intent.setAction(Intent.ACTION_VIEW);
	    intent.setData(uri);
	    startActivity(intent);
	} else if (Utils.fileIsImage(path)) {
	    intent = new Intent();
	    intent.setAction(Intent.ACTION_VIEW);
	    intent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
	    startActivity(intent);
	} else if (Utils.fileIsAudio(path)) {
	    intent = new Intent();
	    intent.setAction(Intent.ACTION_VIEW);
	    intent.setDataAndType(Uri.fromFile(new File(path)), "audio/*");
	    startActivity(intent);
	}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_resources);

	listView = (ListView) findViewById(R.id.listView);
	listView.setOnItemClickListener(this);
	adapter = new ResourcesListAdapter(getApplicationContext(), getLayoutInflater());
	listView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
	super.onResume();
	loadRecentlyViewed();
    }

}
