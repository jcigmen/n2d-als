package com.noobs2d.als;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;

public class Utils {

    public static File[] convertStringCollectionToFileArray(Collection<String> collection) {
	File[] files = new File[collection.size()];
	Iterator<String> iterator = collection.iterator();
	int index = 0;
	while (iterator.hasNext()) {
	    files[index] = new File(iterator.next());
	    index++;
	}
	return files;
    }

    public static boolean fileIsAudio(String path) {
	return path.endsWith(".mp3");
    }

    public static boolean fileIsImage(String path) {
	return path.endsWith(".jpg") || path.endsWith(".png") || path.endsWith(".jpeg");
    }

    public static boolean fileIsPDF(String path) {
	return path.endsWith(".pdf");
    }
}
