package com.noobs2d.als;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.widget.TabHost.TabSpec;

public class MainActivity extends FragmentActivity implements OnClickListener {

    private FragmentTabHost tabHost;

    @Override
    public void onBackPressed() {
	promptExitApplication();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
	if (which == Dialog.BUTTON1)
	    finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_home);

	// set the activity title bar
	setTitle("Alternative Learning System");

	tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
	tabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

	TabSpec learningTab = tabHost.newTabSpec(getResources().getString(R.string.title_section1));
	learningTab.setIndicator(getResources().getString(R.string.title_section1));
	Bundle learningBundle = new Bundle();
	learningBundle.putString("TOP_DIRECTORY", "ALS Learning Resources");

	TabSpec teachingTab = tabHost.newTabSpec(getResources().getString(R.string.title_section2));
	teachingTab.setIndicator(getResources().getString(R.string.title_section2));
	Bundle teachingBundle = new Bundle();
	teachingBundle.putString("TOP_DIRECTORY", "ALS Teaching Resources");

	TabSpec advocacyTab = tabHost.newTabSpec(getResources().getString(R.string.title_section3));
	advocacyTab.setIndicator(getResources().getString(R.string.title_section3));
	Bundle advocacyBundle = new Bundle();
	advocacyBundle.putString("TOP_DIRECTORY", "ALS Advocacy Resources");

	TabSpec referenceTab = tabHost.newTabSpec(getResources().getString(R.string.title_section4));
	referenceTab.setIndicator(getResources().getString(R.string.title_section4));
	Bundle referenceBundle = new Bundle();
	referenceBundle.putString("TOP_DIRECTORY", "ALS Reference Materials");

	TabSpec recentlyViewedTab = tabHost.newTabSpec(getResources().getString(R.string.title_section5));
	recentlyViewedTab.setIndicator(getResources().getString(R.string.title_section5));
	Bundle recentlyViewedBundle = new Bundle();
	recentlyViewedBundle.putString("TOP_DIRECTORY", "Recently Viewed Materials");
	recentlyViewedBundle.putBoolean("SHOW_RECENTLY_VIEWED", true);

	tabHost.addTab(learningTab, ResourcesScreen.class, learningBundle);
	tabHost.addTab(teachingTab, ResourcesScreen.class, teachingBundle);
	tabHost.addTab(advocacyTab, ResourcesScreen.class, advocacyBundle);
	tabHost.addTab(referenceTab, ResourcesScreen.class, referenceBundle);
	tabHost.addTab(recentlyViewedTab, ResourcesScreen.class, recentlyViewedBundle);
    }

    private void promptExitApplication() {
	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	builder.setIconAttribute(android.R.attr.alertDialogIcon);
	builder.setTitle(R.string.prompt_exit_app_title);
	builder.setMessage(R.string.prompt_exit_app_body);
	builder.setPositiveButton("Yes", this);
	builder.setNegativeButton("Cancel", this);
	AlertDialog alert = builder.create();
	alert.show();
    }
}
