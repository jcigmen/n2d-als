package com.noobs2d.als;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;

public class SplashScreen extends Activity {

    public static final long SPLASH_MILLIS = 3000;

    public class SplashRunnable implements Runnable {

	@Override
	public void run() {
	    Intent intent = new Intent(getApplicationContext(), CreditsScreen.class);
	    intent.putExtra("AUTO_TRANSITION", true);
	    startActivity(intent);
	    finish();
	    overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
	}
    }

    private SplashRunnable splashRunnable;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
	// to prevent the OS from thinking the app is hanging
	if (event.getAction() == MotionEvent.ACTION_DOWN)
	    synchronized (splashRunnable) {
		splashRunnable.notifyAll();
	    }
	return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.screen_splash);

	splashRunnable = new SplashRunnable();
	Handler handler = new Handler();
	handler.postDelayed(splashRunnable, SPLASH_MILLIS);
    }
}
